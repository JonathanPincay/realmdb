package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {
    @Required
    private String name;
    @Required
    private String lastName;
    @Required
    private String specialty;
    @Required
    private String mail;
    @Required
    private String age;

    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _name, String _lastName, String _specialty, String _mail, String _age)
    { this.name = _name; this.lastName = _lastName; this.specialty = _specialty; this.mail = _mail; this.age = _age;}
    public Task() {}

    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getLastName() { return lastName; }
    public void setLastName(String name) { this.lastName = lastName; }

    public String getSpecialty() { return specialty; }
    public void setSpecialty(String specialty) { this.specialty = specialty; }

    public String getMail() { return mail; }
    public void setAge(String age) { this.age = age; }
}
